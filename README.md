# soal-shift-sisop-modul-3-D02-2022

## Anggota Kelompok ##

NRP | Nama
---------- | ----------
5025201028 | Muhammad Abror Al Qushoyyi
5025201221 | Naufal Faadhilah
05111940000211 | Vicky Thirdian

## Lapres modul 3

### Kendala

Kami setres

![image](https://media.discordapp.net/attachments/744789471609749570/965259394260553788/pepe_its-removebg-preview.png)

### No1

Soal no 1 diminta untuk mendownload zip file, unzip, melakukan decode isi zip dengan kode base64, dan meletakkan file hasil decoding pada folder hasil, dilengkapi dengan file `no.txt` dan dizip dengan password `mihinomenestuser`

> 1a

melaukan download `music.zip` dan `quote.zip` dari link drive
```C

void zip_download(){
    pid_t child_id;
    int status;

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if (child_id == 0) {
        // this is child
        char *argv[] = {"wget", "-q", "https://drive.google.com/u/0/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download", "-O", "quote.zip", NULL};
        execv("/usr/bin/wget", argv);
        exit(0);
    } else {
        // this is parent
        while ((wait(&status)) > 0);

        char *argv[] = {"wget", "-q", "https://drive.google.com/u/0/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download", "-O", "music.zip", NULL};
        execv("/usr/bin/wget", argv);
        exit(0);
    }
}
```

kemudian dialkukan unzip menggunakan fungsi `unzip_file()`
```c
void unzip_file(char* dir, char* pass){
    pid_t child_id;
    int status;

    char zip[20];
    sprintf(zip, "%s.zip", dir);

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if(child_id == 0){
        if(pass == NULL){
            char* argv[] = {"unzip", "-q","-n", zip, "-d", dir, NULL};
            execv("/bin/unzip",argv);
            exit(0);
        }
        else{
            char* argv[] = {"unzip", "-P", pass, zip, NULL};
            execv("/bin/unzip",argv);
            exit(0);
        }

    }
    else while ((wait(&status)) > 0);
    
}
```

> 1b

mendecode semua file.txt dengan base 64 
```c
const char b64chars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

int b64invs[] = { 62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58,
	59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5,
	6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
	21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28,
	29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42,
	43, 44, 45, 46, 47, 48, 49, 50, 51 };

size_t b64_decoded_size(const char *in)
{
	size_t len;
	size_t ret;
	size_t i;

	if (in == NULL)
		return 0;

	len = strlen(in);
	ret = len / 4 * 3;

	for (i=len; i-->0; ) {
		if (in[i] == '=') {
			ret--;
		} else {
			break;
		}
	}

	return ret;
}

void b64_generate_decode_table()
{
	int    inv[80];
	size_t i;

	memset(inv, -1, sizeof(inv));
	for (i=0; i<sizeof(b64chars)-1; i++) {
		inv[b64chars[i]-43] = i;
	}
}

int b64_isvalidchar(char c)
{
	if (c >= '0' && c <= '9')
		return 1;
	if (c >= 'A' && c <= 'Z')
		return 1;
	if (c >= 'a' && c <= 'z')
		return 1;
	if (c == '+' || c == '/' || c == '=')
		return 1;
	return 0;
}

int b64_decode(const char *in, unsigned char *out, size_t outlen)
{
	size_t len;
	size_t i;
	size_t j;
	int    v;

	if (in == NULL || out == NULL)
		return 0;

	len = strlen(in);
	if (outlen < b64_decoded_size(in) || len % 4 != 0)
		return 0;

	for (i=0; i<len; i++) {
		if (!b64_isvalidchar(in[i])) {
			return 0;
		}
	}

	for (i=0, j=0; i<len; i+=4, j+=3) {
		v = b64invs[in[i]-43];
		v = (v << 6) | b64invs[in[i+1]-43];
		v = in[i+2]=='=' ? v << 6 : (v << 6) | b64invs[in[i+2]-43];
		v = in[i+3]=='=' ? v << 6 : (v << 6) | b64invs[in[i+3]-43];

		out[j] = (v >> 16) & 0xFF;
		if (in[i+2] != '=')
			out[j+1] = (v >> 8) & 0xFF;
		if (in[i+3] != '=')
			out[j+2] = v & 0xFF;
	}

	return 1;
}
```

```c
void *decode(void *file){
    char *filename, buffer[1024], *decoded, loc[20];
    filename = (char *) file;
    size_t len;
    FILE *fptr;
    fptr = fopen(filename, "r");
    fscanf(fptr, "%s", buffer);
    fclose(fptr);

    len = b64_decoded_size(buffer) + 1;
    decoded = (char*) malloc(len*sizeof(char));

    if(b64_decode(buffer, (unsigned char *) decoded, len)){
        decoded[len] = '\0';
        if(filename[0] == 'm') strcpy(loc, "music.txt");
        else if(filename[0] == 'q') strcpy(loc, "quote.txt");
        fptr = fopen(loc, "a");
        fprintf(fptr, "%s\n", decoded);
        fclose(fptr);
    }

    //free(decoded);
}
```

> 1c

memindahkan `file.txt` ke dalam folder hasil
```c
void file_create(char* filename){
    FILE *fptr;
    fptr = fopen(filename, "w");
    if(strcmp(filename, "hasil/no.txt") == 0)fprintf(fptr, "no");
    fclose(fptr);
}

void file_move(char* dest, char* src){
    FILE *fptr1, *fptr2;
    char temp;
    fptr1 = fopen(src, "r");
    fptr2 = fopen(dest, "w");
    while(fscanf(fptr1, "%c", &temp) != EOF) fprintf(fptr2, "%c", temp);
    fclose(fptr1);
    fclose(fptr2);
    remove(src);
}
```

> 1d

Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak)
```c

void zip_file(char *dir){
    pid_t child_id;
    int status;

    char zip[20];
    sprintf(zip, "%s.zip", dir);

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if(child_id == 0){
        char* argv[] = {"zip", "-P","mihinomenestnaufal", "-r", zip, dir, NULL};
        execv("/bin/zip",argv);
        exit(0);
    }
    else while ((wait(&status)) > 0);
}
```

> 1e
unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.
```c
void unzip_file(char* dir, char* pass){
    pid_t child_id;
    int status;

    char zip[20];
    sprintf(zip, "%s.zip", dir);

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if(child_id == 0){
        if(pass == NULL){
            char* argv[] = {"unzip", "-q","-n", zip, "-d", dir, NULL};
            execv("/bin/unzip",argv);
            exit(0);
        }
        else{
            char* argv[] = {"unzip", "-P", pass, zip, NULL};
            execv("/bin/unzip",argv);
            exit(0);
        }

    }
    else while ((wait(&status)) > 0);
    
}
```
semua fungsi diatas dijlankan pada `main function` dengan menggunakan thread
```c
int main(){
    pthread_t thread[9];
    int  iret[9];
    char file_loc[50];

    pid_t child_id;
    int status;

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if (child_id == 0) {
        zip_download();
    }
    else{
        while ((wait(&status)) > 0);
        create_dir("quote");
        create_dir("music");
        unzip_file("quote", NULL);
        unzip_file("music", NULL);
        file_create("quote.txt");
        file_create("music.txt");
    }

    for(int i=1; i<10; i++){
        sprintf(file_loc, "music/m%d.txt", i);
        iret[i-1] = pthread_create(&thread[i-1], NULL, decode, (void*) file_loc);
        if(iret[i-1]){
            fprintf(stderr,"Error - pthread_create() return code: %d\n",iret[i-1]);
            exit(EXIT_FAILURE);
        }
        pthread_join( thread[i-1], NULL);
    }

    for(int i=1; i<10; i++){
        sprintf(file_loc, "quote/q%d.txt", i);
        iret[i-1] = pthread_create(&thread[i-1], NULL, decode, (void*) file_loc);
        if(iret[i-1]){
            fprintf(stderr,"Error - pthread_create() return code: %d\n",iret[i-1]);
            exit(EXIT_FAILURE);
        }
        pthread_join( thread[i-1], NULL);
    }

    create_dir("hasil");
    file_move("hasil/music.txt", "music.txt");
    file_move("hasil/quote.txt", "quote.txt");
    zip_file("hasil");
    unzip_file("hasil.zip", "mihinomenestnaufal");
    file_create("hasil/no.txt");
    zip_file("hasil");
    

    exit(EXIT_SUCCESS);
}
```

### No2

Pada soal praktikum nomor 2 ini secara ringkas diperintahkan untuk membuat sistem client-server. User pada sisi client akan memilih terlebih dahulu antara register dan login. Apabila user memilih register, maka client side akan meminta username dan password yang secara otomatis data tersebut akan tersimpan di users.txt guna untuk sisi server dalam mencari data user. Jika user memilih login maka user dapat melakukan fitur-fitur command seperti add, see,download, submit

Berikut merupakan code jika user memilih register. Yang mana pada sisi server akan mengecek username dan password pada file users.txt.

Client Side
```
void regist(){
    bool p_num = true, p_up = true, p_low = true, u_=false;
    char c, user[sz]={}, pass[sz]={}, temp_buff[sz10b]={};
    char data_user[sz10b][sz]={};
    int idx, i;
    valread = read(sock, temp_buff, sz10b);
    idx = convertStringToInt(temp_buff);
    valread = read(sock, data_user, sizeof(data_user));
    printf("Server free, you may type now\n");
    printf("Username: ");
    while(!u_){
        scanf("%s", user);
        for (i = 0; i < idx; i++){
            if (!strcmp(data_user[i], user)){
                u_=true;
                break;
            }
        }
        if (u_){
            u_ = false;
            printf("Username already exist\nUsername: ");
        }else {
            u_ = true;
            getchar();
        }
    }

    printf("Password: ");
    while(p_num || p_low || p_up) {
        c = getchar();
        while (c > 31){
            if (c == 32) {c = getchar(); continue;}
            if (p_num == true && c>47 && c<58) p_num = false;
            if (p_up == true && c>64 && c<91) p_up = false;
            if (p_low == true && c>96 && c<123) p_low = false;
            strncat(pass, &c, 1);
            c = getchar();
        }
        if(p_num || p_low || p_up || strlen(pass) < 6){
            printf("\nPassword Incorrect, password must contain the following:\n");
            if (strlen(pass) < 6) printf("- Minimum 6 characters\n");
            if (p_num) printf("- A  number\n");
            if (p_low) printf("- A  lowercase letter\n");
            if (p_up) printf("- An uppercase letter\n");
            p_num = p_up = p_low = true; 
            memset(pass, '\0', sz);
            printf("Password: ");
        }
    }
    write(sock, user, sz10b);
    write(sock, pass, sz10b);
}
```
Server Side

```
void registHandler(int new_socket){
    char line[sz10b], data_user[sz10b][sz]={};
    int idx=0;
    FILE* fp = fopen(users_file,"r");
	if(!fp) {
        printf("Unable to open %s\n", users_file);
    }else {
        printf("Username List:\n"); 
        while (fgets(line, sizeof(line), fp)) {
            char *u;
            const char s[2] = ":";
            u = strtok(line, s);
            printf("-%s\n", u); 
            strcpy(data_user[idx], u);
            idx++;
        }
        fclose(fp);
    }
    char temp_buff[sz10b]={};
    sprintf(temp_buff, "%d", idx);
    write(new_socket, temp_buff, sz10b);
    write(new_socket, data_user, sizeof(data_user));
    
    char user[sz10b]={}, 
         pass[sz10b]={};

    valread = read(new_socket, user, sz10b);
    valread = read(new_socket, pass, sz10b);
    fp = fopen(users_file,"a");
    fprintf(fp, "%s:%s\n", user, pass);
    printf("User [%s] Registered\n", user);
    fclose(fp);
}
```
Selanjutnya database pada server akan menampung problem yang akan dimasukkan. Database itu dinamakan problems.tsv yang isinya ialah judul dari problem dan author problem yang dipisah dengan \t. Berikut merupakan fungsi untuk membuat file tsv tersebut

Server side
```
void create_tsv(){
    FILE* fp;
    if (fp = fopen(database, "r")){
    }else{
        fp = fopen(database, "a");
        fprintf(fp, "Judul\tAuthor\n");
    }
    fclose(fp);
}
```
Selanjutnya user dapat melakukan command add pada sisi client yang dapat diisi dengan judul problem, path file deskripsinya, path file input, dan path file output

Server side
```
int _command(int new_socket){
    char com[sz]={};
    valread = read(new_socket, com, sizeof(com));
    if (!strcmp(com, "add")){
        printf("User [%s] adding a problem\n", userlog);
        FILE* fp = fopen(database, "a");
        char question[4][sz] = {"Judul problem:", "Filepath description.txt:", "Filepath input.txt:", "Filepath output.txt:"};
        write2d(question, 4, new_socket);
        char serv_path[sz*2], client_path[sz*2], prob[sz]={};
        read(new_socket, client_path, sizeof(client_path));
        for(int i=0; i<4; i++){
            char buffer[sz10b]={}, src_path[sz10b]={}, dest_path[sz10b]={};
            valread = read(new_socket, buffer, sizeof(buffer));
            if (i == 0){
                struct stat st = {};
                while (stat(buffer, &st) != -1) {
                    write(new_socket, "error", sz);
                    valread = read(new_socket, buffer, sizeof(buffer));
                }
                if (stat(buffer, &st) == -1) write(new_socket, "success", sz);
                fprintf(fp, "%s\t%s\n", buffer, userlog);
                strcpy(prob, buffer);
                mkdir(buffer, 0700);
                chdir(buffer);
                if (getcwd(serv_path, 100*2) == NULL){
                    perror("error\n");
                }
...
```
Lalu user(client) juga dapat menggunakan command see yang mana ini akan menunjukkan judul problem dan author problem. berikut merupakan potongan code-nya pada sisi server
```
...
else if (!strcmp(com, "see")){
        printf("User [%s] see problem list\n", userlog);
        FILE* fp = fopen(database, "r");
        char author[sz10b][sz]={}, problem[sz10b][sz]={};
        fscanf(fp, "%s %s\n", problem[0], author[0]);
        int idx = 0;
        while(fscanf(fp, "%s\t%s", problem[idx], author[idx]) == 2){
            idx++;
        }
        writeInt(idx, sz, new_socket);
        for(int i=0; i<idx; i++){
            write(new_socket, problem[i], sz);
            write(new_socket, author[i], sz);
        }
        fclose(fp);
        return 1;
    }
...
```

Melakukan command download dengan memilih salah satu judul problem yang sudah diinput

Client side
```
...
else if (!strcmp(com, "download")){
        char problem[sz]={}, path[sz*2]={}, message[sz10b]={};
        scanf("%s", problem);
        write(sock, problem, sz);
        getcwd(path, sz*2);
        write(sock, path, sz*2);
        valread = read(sock, message, sz10b);
        printf("%s\n\n", message);
        return 1;
    }
...
```

Server side
```
void downloadFile(char *src_path, char *dst_path){
    int src_fd, dst_fd, n, err;
    unsigned char buffer[sz10b*10];

    src_fd = open(src_path, O_RDONLY);
    dst_fd = open(dst_path, O_CREAT | O_WRONLY);

    while (1) {
        err = read(src_fd, buffer, sz10b*10);
        if (err == -1) {
            printf("Error reading file.\n");
            exit(1);
        }
        n = err;

        if (n == 0) break;

        err = write(dst_fd, buffer, n);
        if (err == -1) {
            printf("Error writing to file.\n");
            exit(1);
        }
    }
    close(src_fd);
    close(dst_fd);
}
```

Lalu juga dapat melakukan submit dengan cara memasukkan file path dari output.txt 
```
...
else if (!strcmp(com, "submit")){
        char problem[sz]={}, path[sz*2]={}, curr_path[sz*2], message[sz];
        scanf("%s%s", problem, path);
        getcwd(curr_path, sz*2);
        write(sock, problem, sz);
        strcat(curr_path, "/");
        strcat(curr_path, path);
        write(sock, curr_path, sz*2);
        valread = read(sock, message, sz);
        printf("%s\n\n", message);
        return 1;
    }
...
```
Agar server dapat menangani multiple connection. Dimana jika terdapat 2 atau lebih client yang terhubung ke server, maka harus menunggu sampai client pertama keluar untuk bisa melakukan login dan mengakses aplikasinya
```
...
while((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))){
        printf("Connection accepted\n");

        pthread_t sniffer_thread;
		int *new_sock = malloc(1);
		*new_sock = new_socket;
		
		if( pthread_create( &sniffer_thread , NULL ,  client_connect_handler , (void*) new_sock) < 0){
			perror("could not create thread");
			return 1;
		}
		
		pthread_join(sniffer_thread ,NULL);
		printf("\nDisconnected from client\n");
        printf("Waiting for client connections...\n");
    }
...
```


### No3

> 3a 3b 3c

melakukan extrak file `hartakarun.zip` dari link drive kemudian diletakkan kedalam direktori `home/user/shift/hartakarun/` karena menggunakan windows maka diletakkan kedalam direktori `/mnt/c/Users/asus/shift3/hartakarun`

```c

pthread_t tid[3]; //inisialisasi array untuk menampung thread dalam kasus ini ada 2 thread
pid_t child;

int length=3; //inisialisasi jumlah untuk looping
void* playandcount(void *arg)
{
	int status;
	char *argv[] = {"wget", NULL};
	char *argv1[] = {"mkdir", NULL};
	char *argv3[] = {"unzip", NULL};
	unsigned long i=0;
	pthread_t id=pthread_self();
	int iter;
	if(pthread_equal(id,tid[0])) //thread untuk clear layar
	{
		child = fork();
		if (child == 0){
			char *argv[] = {"wget", "-q", "https://drive.google.com/u/0/uc?id=1ewbXlCq-sh80WafFaoPjy2aOGIVeO-4W&export=download", "-O", "hartakarun.zip", NULL};
       		execv("/usr/bin/wget", argv);
        	exit(0);
		}
		else if (child > 0){
			while((wait(&status))>0);
			child = fork();
			
			if(child == 0){
				char *argv2[] = {"mkdir", "-p", "/mnt/c/Users/asus/shift3", NULL};
				execv("/bin/mkdir", argv2);
				exit(0);
			}
			else if (child > 0){
				// while((wait(&status))>0);
				child = fork();
				if (child == 0){
					char* argv3[] = {"unzip","-q","-n","hartakarun.zip", "-d", "/mnt/c/Users/asus/shift3", NULL};
					execv("/usr/bin/unzip", argv3);
					exit(0);
				}
				else {
					while((wait(&status))>0);

					if ((chdir("/mnt/c/Users/asus/shift3/")) < 0)exit(EXIT_FAILURE);
				}
			}
		}
	}
	return NULL;
}
```
> 3d

melakukan pengiriman file menggunakan client-server

```c
void hartakarun_zip(){
    char* argv[]={"zip","-P", "hartakarun.zip", "-d", "/mnt/c/Users/asus/shift3/*"};
    execv("/usr/bin/zip", argv);
    exit(0);
}
```
file di zip terlebih dahulu didalam `client.c`

```c
int main () {
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    // char *hello = "Hello from client";
    char buffer[1024] = {0};

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
    memset(&serv_addr, '0', sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }

    while (1) {
        char *path;
        char *filename;
        path = "/mnt/c/Users/asus/Downloads/SISOP D/modul3/soal-shift-modul-3-d02-2022/hartakarun.zip";
        filename = strrchr(path,'/')+1;
        hartakarun_zip();
        char *input;
        scanf("%s", input);
        send(sock, input, strlen(input), 0);
        if (strcmp(input, "sendhartakarun") == 0) {
            send(sock, filename, sizeof(filename), 0);// ngirim ke server
        }

        valread = read(sock, buffer, 1024);// buffer : lawannya
        printf("%s\n", buffer);
        memset(buffer, 0, 1024);
    }
    return 0;
}
```
melakukan pengiriman menggunakan socket dengan menggunakan keyword `send hartakarun.zip`

> 3e

server menerima file hartakarun.zip dari client ketika ada input string `send hartakarun.zip`

```c
int main () {
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {};
    int numbers[100];

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    int index = 0;
    while (1) {
        valread = read(new_socket, buffer, 1024);

        if (strcmp(buffer, "sendhartakarun") == 0) {
            memset(buffer, 0, 1024);
            valread = read(new_socket, buffer, 1024);

            int num = atoi(buffer);
            numbers[index] = num;
            printf("%d\n", num);
            index++;

            char *msg = "send file succes";

            send(new_socket, msg, strlen(msg), 0);
        } else {
            char *msg = "Bad Request";
            send(new_socket, msg, strlen(msg), 0);
        }

        memset(buffer, 0, 1024);
    }
    return 0;
}
```

![image](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRiWb2PhUQFUmyEc0mjs2wMKOj-cFcasQcXyw&usqp=CAU)

