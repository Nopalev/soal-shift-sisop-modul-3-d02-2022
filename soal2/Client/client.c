#include <stdio.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#define PORT 8080
#define sz 100
#define sz10b 1024
int sock = 0, valread, auth_ = 0;
char userlog[sz]={};


void read2d(char arr[][sz], int idx){
    for (int i=0; i<idx; i++){
        read(sock, arr[i], sizeof(arr[i]));
    }
}

void write2d(char arr[][sz], int idx){
    for (int i=0; i<idx; i++){
        write(sock, arr[i], sizeof(arr[i]));
    }
}

int convertStringToInt(char s[]){
    int len = strlen(s);
    int idx=0;
    for(int i = 0; i<len; i++){
        idx = idx * 10 + (s[i]-'0');
    }
    return idx;
}

void login(){
    int status;
    char user[sz10b], pass[sz10b], temp_buff[sz10b]={}, data_user[sz10b][sz]={}, pass_user[sz10b][sz]={};
    int idx;
    valread = read(sock, temp_buff, sz10b);
    idx = convertStringToInt(temp_buff);
    read2d(data_user, idx);

    for (int i=0; i<idx; i++){
        char *u, *p;
        u = strtok(data_user[i], ":");
        p = strtok(NULL, "\n");
        strcpy(data_user[i], u);
        strcpy(pass_user[i], p);
    }
    
    printf("Server free, you may type now\n");
    while(!auth_){
        int auth_idx=-1;
        while(auth_idx == -1){
            printf("Username: ");
            scanf("%s", user);
            for (int i = 0; i < idx; i++){

                if (!strcmp(data_user[i], user)){
                    auth_idx = i;
                    break;
                }
            }
        }

        printf("Password: ");
        scanf("%s", pass);
        if (!strcmp(pass, pass_user[auth_idx])){
            strcpy(userlog, data_user[auth_idx]);
            auth_ = 1;
            printf("Login Succesful\n");
        }else {
            printf("Failed to login -%s-\n", pass_user[auth_idx]);
        }
            

    }
}

void regist(){
    bool p_num = true, p_up = true, p_low = true, u_=false;
    char c, user[sz]={}, pass[sz]={}, temp_buff[sz10b]={};
    char data_user[sz10b][sz]={};
    int idx, i;
    valread = read(sock, temp_buff, sz10b);
    idx = convertStringToInt(temp_buff);
    valread = read(sock, data_user, sizeof(data_user));
    printf("Server free, you may type now\n");
    printf("Username: ");
    while(!u_){
        scanf("%s", user);
        for (i = 0; i < idx; i++){
            if (!strcmp(data_user[i], user)){
                u_=true;
                break;
            }
        }
        if (u_){
            u_ = false;
            printf("Username already exist\nUsername: ");
        }else {
            u_ = true;
            getchar();
        }
    }

    printf("Password: ");
    while(p_num || p_low || p_up) {
        c = getchar();
        while (c > 31){
            if (c == 32) {c = getchar(); continue;}
            if (p_num == true && c>47 && c<58) p_num = false;
            if (p_up == true && c>64 && c<91) p_up = false;
            if (p_low == true && c>96 && c<123) p_low = false;
            strncat(pass, &c, 1);
            c = getchar();
        }
        if(p_num || p_low || p_up || strlen(pass) < 6){
            printf("\nPassword Incorrect, password must contain the following:\n");
            if (strlen(pass) < 6) printf("- Minimum 6 characters\n");
            if (p_num) printf("- A  number\n");
            if (p_low) printf("- A  lowercase letter\n");
            if (p_up) printf("- An uppercase letter\n");
            p_num = p_up = p_low = true; 
            memset(pass, '\0', sz);
            printf("Password: ");
        }
    }
    write(sock, user, sz10b);
    write(sock, pass, sz10b);
}

void auth(){
    char choice[1];
    printf("choose\n- r to register\n- l to login\n");
    scanf("%s", choice);
    write(sock, choice, sz10b);
    if (choice[0] == 'r'){
        regist();
    }else if (choice[0] == 'l'){
        login();
    }
}  

void writeInt(int var, int size){
    char buffer[sz10b]={};
    sprintf(buffer, "%d", var);
    write(sock, buffer, size);
}

int command(){
    char com[sz]={};
    printf("Command list\nadd\nsee\ndownload <problem_name>\nsubmit <problem_name> <output.txt_path_relative_to_source_code>\nexit\nCommand: ");
    scanf("%s", com);
    write(sock, com, sizeof(com));
    if (!strcmp(com, "add")){
        char question[4][sz]={0};
        char client_dir[sz*2];
        getcwd(client_dir, sz*2);
        read2d(question, 4);
        write(sock, client_dir, sizeof(client_dir));
        for(int i=0; i<4; i++){
            char in[sz]={}, path[sz]={};
            printf("%s ", question[i]);
            scanf("%s", in);
            write(sock, in, sizeof(in));
            if (i==0){
                char temp[sz]={};
                read(sock, temp, sz);
                while(!(strcmp(temp, "error"))){
                    printf("Problem exist!\n%s ", question[i]);
                    scanf("%s", in);
                    write(sock, in, sizeof(in));
                    read(sock, temp, sz);
                }
            }
        }
        printf("\n");
        return 1;
    }else if (!strcmp(com, "see")){
        char author[sz], problem[sz], buffer[sz];
        printf("Problem list:\n");
        valread = read(sock, buffer, sz);
        int idx = convertStringToInt(buffer);
        for(int i=0; i<idx; i++){
            read(sock, problem, sz);
            read(sock, author, sz);
            printf("-> %s by %s\n", problem, author);
        }
        printf("\n");
        return 1;
    }else if (!strcmp(com, "download")){
        char problem[sz]={}, path[sz*2]={}, message[sz10b]={};
        scanf("%s", problem);
        write(sock, problem, sz);
        getcwd(path, sz*2);
        write(sock, path, sz*2);
        valread = read(sock, message, sz10b);
        printf("%s\n\n", message);
        return 1;
    }else if (!strcmp(com, "submit")){
        char problem[sz]={}, path[sz*2]={}, curr_path[sz*2], message[sz];
        scanf("%s%s", problem, path);
        getcwd(curr_path, sz*2);
        write(sock, problem, sz);
        strcat(curr_path, "/");
        strcat(curr_path, path);
        write(sock, curr_path, sz*2);
        valread = read(sock, message, sz);
        printf("%s\n\n", message);
        return 1;
    }else if (!strcmp(com, "exit")){
        return 0;
    }else {
        printf("Command %s not available\n\n", com);
        return 1;
    }
}

int main(int argc, char const *argv[]) {
    struct sockaddr_in address;
    struct sockaddr_in serv_addr;
    char *hello = "Hello from client";
    char buffer[sz10b] = {0};
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
    
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }

    auth();
    writeInt(auth_, sz10b);
    write(sock, userlog, sizeof(userlog));
    if(auth_){
        while(command());
    }
    close(sock);
    return 0;
}