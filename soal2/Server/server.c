#include <stdio.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h> 
#define PORT 8080
#define sz10b 1024
#define sz 100

int valread;
char *users_file = "users.txt", *database = "problems.tsv";
char userlog[sz]={};

void downloadFile(char *src_path, char *dst_path){
    int src_fd, dst_fd, n, err;
    unsigned char buffer[sz10b*10];

    src_fd = open(src_path, O_RDONLY);
    dst_fd = open(dst_path, O_CREAT | O_WRONLY);

    while (1) {
        err = read(src_fd, buffer, sz10b*10);
        if (err == -1) {
            printf("Error reading file.\n");
            exit(1);
        }
        n = err;

        if (n == 0) break;

        err = write(dst_fd, buffer, n);
        if (err == -1) {
            printf("Error writing to file.\n");
            exit(1);
        }
    }
    close(src_fd);
    close(dst_fd);
}

void write2d(char arr[][sz], int idx, int new_socket){
    for (int i=0; i<idx; i++){
        write(new_socket, arr[i], sizeof(arr[i]));
    }
}

int convertStringToInt(char s[], int new_socket){
    int len = strlen(s);
    int idx=0;
    for(int i = 0; i<len; i++){
        idx = idx * 10 + (s[i]-'0');
    }
    return idx;
}

void loginHandler(int new_socket){
    FILE* fp = fopen(users_file,"r");
	if(!fp) {
        printf("Unable to open %s\n", users_file);
        exit(0);
    }
    char line[sz10b], data_user[sz10b][sz]={};
    int idx=0;
    printf("Username and password list:\n");
    while (fgets(line, sz10b, fp)) {
        char *u;
        u = strtok(line, "\n");
        printf("-%s-\n", u); 
        strcpy(data_user[idx], u);
        idx++;
    }
    fclose(fp);
    char temp_buff[sz10b]={};
    sprintf(temp_buff, "%d", idx);
    write(new_socket, temp_buff, sz10b);
    write2d(data_user, idx, new_socket);
}

void regis_handler(int new_socket){
    char line[sz10b], data_user[sz10b][sz]={};
    int idx=0;
    FILE* fp = fopen(users_file,"r");
	if(!fp) {
        printf("Unable to open %s\n", users_file);
    }else {
        printf("Username List:\n"); 
        while (fgets(line, sizeof(line), fp)) {
            char *u;
            const char s[2] = ":";
            u = strtok(line, s);
            printf("-%s\n", u); 
            strcpy(data_user[idx], u);
            idx++;
        }
        fclose(fp);
    }
    char temp_buff[sz10b]={};
    sprintf(temp_buff, "%d", idx);
    write(new_socket, temp_buff, sz10b);
    write(new_socket, data_user, sizeof(data_user));
    
    char user[sz10b]={}, 
         pass[sz10b]={};

    valread = read(new_socket, user, sz10b);
    valread = read(new_socket, pass, sz10b);
    fp = fopen(users_file,"a");
    fprintf(fp, "%s:%s\n", user, pass);
    printf("User [%s] Registered\n", user);
    fclose(fp);
}

void authHandler(int new_socket){
    char choice[sz10b];
    valread = read(new_socket , choice, sz10b);
    if (choice[0] == 'r'){
        regis_handler(new_socket);
    }else if (choice[0] == 'l'){
        loginHandler(new_socket);
    }
}

void writeInt(int var, int size, int new_socket){
    char buffer[sz]={};
    sprintf(buffer, "%d", var);
    write(new_socket, buffer, size);
}

int _command(int new_socket){
    char com[sz]={};
    valread = read(new_socket, com, sizeof(com));
    if (!strcmp(com, "add")){
        printf("User [%s] adding a problem\n", userlog);
        FILE* fp = fopen(database, "a");
        char question[4][sz] = {"Judul problem:", "Filepath description.txt:", "Filepath input.txt:", "Filepath output.txt:"};
        write2d(question, 4, new_socket);
        char serv_path[sz*2], client_path[sz*2], prob[sz]={};
        read(new_socket, client_path, sizeof(client_path));
        for(int i=0; i<4; i++){
            char buffer[sz10b]={}, src_path[sz10b]={}, dest_path[sz10b]={};
            valread = read(new_socket, buffer, sizeof(buffer));
            if (i == 0){
                struct stat st = {};
                while (stat(buffer, &st) != -1) {
                    write(new_socket, "error", sz);
                    valread = read(new_socket, buffer, sizeof(buffer));
                }
                if (stat(buffer, &st) == -1) write(new_socket, "success", sz);
                fprintf(fp, "%s\t%s\n", buffer, userlog);
                strcpy(prob, buffer);
                mkdir(buffer, 0700);
                chdir(buffer);
                if (getcwd(serv_path, 100*2) == NULL){
                    perror("error\n");
                }
            }else{
                strcat(dest_path, serv_path);
                strcat(dest_path, "/");
                strcat(dest_path, buffer);
                strcat(src_path, client_path);
                strcat(src_path, "/");
                strcat(src_path, buffer);
                downloadFile(src_path, dest_path);
            }
        }
        chdir("..");
        printf("Problem [%s] has been added by [%s]\n", prob, userlog);
        fclose(fp);
        return 1;
    }else if (!strcmp(com, "see")){
        printf("User [%s] see problem list\n", userlog);
        FILE* fp = fopen(database, "r");
        char author[sz10b][sz]={}, problem[sz10b][sz]={};
        fscanf(fp, "%s %s\n", problem[0], author[0]);
        int idx = 0;
        while(fscanf(fp, "%s\t%s", problem[idx], author[idx]) == 2){
            idx++;
        }
        writeInt(idx, sz, new_socket);
        for(int i=0; i<idx; i++){
            write(new_socket, problem[i], sz);
            write(new_socket, author[i], sz);
        }
        fclose(fp);
        return 1;
    }else if (!strcmp(com, "download")){
        printf("User [%s] download a problem\n", userlog);
        char problem[sz]={}, client_path[sz*2]={}, desc_path[sz*2]={}, in_path[sz*2]={}, serv_path[sz*2]={}, i_c_path[sz*2]={}, d_c_path[sz*2]={}, message[sz10b]={};
        FILE* fp; 
        valread = read(new_socket, problem, sz);
        valread = read(new_socket, client_path, sz*2);
        getcwd(serv_path, sz*2);
        strcat(desc_path, serv_path);
        strcat(desc_path, "/");
        strcat(desc_path, problem);
        strcat(desc_path, "/");
        strcat(in_path, desc_path);
        strcat(desc_path, "description.txt");
        strcat(in_path, "input.txt");
        if((fp = fopen(in_path, "r")) == NULL){
            strcpy(message, "Problem doesn't exist!");
            write(new_socket, message, sz10b);
            return 1;
        }
        strcat(client_path, "/");
        strcat(client_path, problem);
        mkdir(client_path, 0700);
        strcat(i_c_path, client_path);
        strcat(i_c_path, "/input.txt");
        strcat(d_c_path, client_path);
        strcat(d_c_path, "/description.txt");
    
        if((fp = fopen(i_c_path, "r")) != NULL){
            fclose(fp);
            strcpy(message, "Problem has been already downloaded!");
            write(new_socket, message, sz10b);
            return 1;
        }
        downloadFile(in_path, i_c_path);
        downloadFile(desc_path, d_c_path);
        strcpy(message, "Problem successfully downloaded!");
        write(new_socket, message, sz10b);
        printf("Problem [%s] successfully downloaded by [%s]\n", problem, userlog);
        return 1;
    }else if (!strcmp(com, "submit")){
        printf("User [%s] submit a problem\n", userlog);
        char problem[sz]={}, path[sz*2]={}, curr_path[sz*2]={};
        valread = read(new_socket, problem, sz);
        valread = read(new_socket, path, sz*2);
        getcwd(curr_path, sz*2);
        strcat(curr_path, "/");
        strcat(curr_path, problem);
        strcat(curr_path, "/");
        strcat(curr_path, "output.txt");
        FILE* fp = fopen(curr_path,"r");
        if(!fp) {
            printf("Unable to open %s\n", curr_path);
            exit(0);
        }
        char serv_out[sz10b][sz10b]={}, cli_out[sz10b][sz10b]={}, line[sz10b]={};
        int idx_serv=0, idx_cli=0;
        while (fgets(line, sizeof(line), fp)) {
            char *u; 
            u = strtok(line, "\n");
            strcpy(serv_out[idx_serv], u);
            idx_serv++;
        }fclose(fp);
        fp = fopen(path,"r");
        if(!fp) {
            printf("Unable to open %s\n", path);
            exit(0);
        }
        while (fgets(line, sizeof(line), fp)) {
            char *u; 
            u = strtok(line, "\n");
            strcpy(cli_out[idx_cli], u);
            idx_cli++;
        }fclose(fp);
        char message[sz]={};
        if(idx_serv != idx_cli) {
            strcpy(message, "WA");
            write(new_socket, message, sz);
            return 1;
        }
        for (int i=0; i<idx_serv; i++){
            printf("-%s-%s-\n", serv_out[i], cli_out[i]);
            if(strcmp(serv_out[i], cli_out[i])){
                strcpy(message, "WA");
                write(new_socket, message, sz);
                return 1;
            }
        }
        strcpy(message, "AC");
        write(new_socket, message, sz);
        return 1;
    }
    return 0;
} 

void create_tsv(){
    FILE* fp;
    if (fp = fopen(database, "r")){
    }else{
        fp = fopen(database, "a");
        fprintf(fp, "Judul\tAuthor\n");
    }
    fclose(fp);
}

void *client_connect_handler(void *sock){
    int new_socket = *(int*)sock;
    int auth_ = 0;
    char buffer[sz10b] = {};
    create_tsv();
    authHandler(new_socket);
    valread = read(new_socket, buffer, sz10b);
    auth_ = convertStringToInt(buffer, new_socket);
    valread = read(new_socket, userlog, sizeof(userlog));
    if (auth_){
        printf("User [%s] logged in\n", userlog);
        while(_command(new_socket));
    }
    free(sock);
}

int main(int argc, char const *argv[]) {
    int server_fd;
    struct sockaddr_in address;
    int opt = 1, new_socket;
    int addrlen = sizeof(address);
    
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt failed");
        exit(EXIT_FAILURE);
    }
    
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = htonl(INADDR_ANY);
    address.sin_port = htons( PORT );
    
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))< 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen failed");
        exit(EXIT_FAILURE);
    }

    printf("Waiting for client connections...\n");
    while((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))){
        printf("Connection accepted\n");

        pthread_t sniffer_thread;
		int *new_sock = malloc(1);
		*new_sock = new_socket;
		
		if( pthread_create( &sniffer_thread , NULL ,  client_connect_handler , (void*) new_sock) < 0){
			perror("could not create thread");
			return 1;
		}
		
		pthread_join(sniffer_thread ,NULL);
		printf("\nDisconnected from client\n");
        printf("Waiting for client connections...\n");
    }
    
    if (new_socket < 0) {
        perror("accept failed");
        exit(EXIT_FAILURE);
    }

    return 0;
}
