#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#define PORT 8080
 
void hartakarun_zip(){
    char* argv[]={"zip","-P", "hartakarun.zip", "-d", "/mnt/c/Users/asus/shift3/*"};
    execv("/usr/bin/zip", argv);
    exit(0);
}

int main () {
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    // char *hello = "Hello from client";
    char buffer[1024] = {0};

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
    memset(&serv_addr, '0', sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }

    while (1) {
        char *path;
        char *filename;
        path = "/mnt/c/Users/asus/Downloads/SISOP D/modul3/soal-shift-modul-3-d02-2022/hartakarun.zip";
        filename = strrchr(path,'/')+1;
        hartakarun_zip();
        char *input;
        scanf("%s", input);
        send(sock, input, strlen(input), 0);
        if (strcmp(input, "sendhartakarun") == 0) {
            send(sock, filename, sizeof(filename), 0);// ngirim ke server
        }

        valread = read(sock, buffer, 1024);// buffer : lawannya
        printf("%s\n", buffer);
        memset(buffer, 0, 1024);
    }
    return 0;
}
