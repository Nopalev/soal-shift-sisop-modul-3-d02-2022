// C program to demonstrate use of fork() and pipe() 
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <sys/types.h> 
#include <pthread.h>
#include <ctype.h>
#include <dirent.h>
#include <sys/wait.h> 
#include <string.h>
#include <pthread.h>
#include <sys/stat.h>


pthread_t tid[3]; //inisialisasi array untuk menampung thread dalam kasus ini ada 2 thread
pid_t child;

int length=3; //inisialisasi jumlah untuk looping
void* playandcount(void *arg)
{
	int status;
	char *argv[] = {"wget", NULL};
	char *argv1[] = {"mkdir", NULL};
	char *argv3[] = {"unzip", NULL};
	unsigned long i=0;
	pthread_t id=pthread_self();
	int iter;
	if(pthread_equal(id,tid[0])) //thread untuk clear layar
	{
		child = fork();
		if (child == 0){
			char *argv[] = {"wget", "-q", "https://drive.google.com/u/0/uc?id=1ewbXlCq-sh80WafFaoPjy2aOGIVeO-4W&export=download", "-O", "hartakarun.zip", NULL};
       		execv("/usr/bin/wget", argv);
        	exit(0);
		}
		else if (child > 0){
			while((wait(&status))>0);
			child = fork();
			
			if(child == 0){
				char *argv2[] = {"mkdir", "-p", "/mnt/c/Users/asus/shift3", NULL};
				execv("/bin/mkdir", argv2);
				exit(0);
			}
			else if (child > 0){
				// while((wait(&status))>0);
				child = fork();
				if (child == 0){
					char* argv3[] = {"unzip","-q","-n","hartakarun.zip", "-d", "/mnt/c/Users/asus/shift3", NULL};
					execv("/usr/bin/unzip", argv3);
					exit(0);
				}
				else {
					while((wait(&status))>0);

					if ((chdir("/mnt/c/Users/asus/shift3/")) < 0)exit(EXIT_FAILURE);
				}
			}
		}
	}
	return NULL;
}


//fungsi buat cek eksis file
int file_c_exists(const char *nama_file)
{
    struct stat buffer;
    int tersedia = stat(nama_file, &buffer);
    if (tersedia != 0)
        return 0;
    else // -1
        return 1;
}

//fungsi pindah
void *pindah(void *nama_file)
{
    char cwd[PATH_MAX];
	char nama_direktori[200];
	char hidden[100], hiddenname[100];
	char file[100], existsfile[100];
    int i;

    strcpy(hiddenname, nama_file);
	strcpy(existsfile, nama_file);
    
    char *namaa = strrchr(hiddenname, '/');
    strcpy(hidden, namaa);

    //Untuk file yang hidden, awalan .
    if (hidden[1] == '.'){
		strcpy(nama_direktori, "Hidden");
	}
        
    
    //File biasa
    else if (strstr(nama_file, ".") != NULL)
    {
        strcpy(file, nama_file);
        strtok(file, ".");
        char *point = strtok(NULL, "");
        //nggak case sensitive
        for (i = 0; point[i]; i++)
        	point[i] = tolower(point[i]);
        
        strcpy(nama_direktori, point);
    }
    //file ga berextensi
    else
        strcpy(nama_direktori, "Unknown");
    

    //cek file ada ato ga, kalo ga dibuat folder
    int tersedia = file_c_exists(existsfile);
	// char* dirnme = "hartakarun";
    if (tersedia)		
        mkdir(nama_direktori,  755);
	
	// getch();

    //nama_file didapat
    if (getcwd(cwd, sizeof(cwd)) != NULL)
    {
        char *nama = strrchr(nama_file, '/');
        char namafile[200];

        strcpy(namafile, cwd);

        strcat(namafile, "/");
		strcat(namafile, nama_direktori);
        strcat(namafile, nama);
		
        

        // file dinpidah pake rename
        rename(nama_file, namafile);
    }
}

//list rekursif
void fileRekursif(char *dasarPath)
{
    char path[1000];
    struct stat buffer;
	struct dirent *dp;
    
    DIR *dir = opendir(dasarPath);
    int n = 0;

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            // Construct new path from our base path
            strcpy(path, dasarPath);

			strcat(path, "/");
			strcat(path, dp->d_name);
            
            

            if (stat(path, &buffer) == 0 && S_ISREG(buffer.st_mode))
            {
                //membuat thread untuk cek pindah
                pthread_t thread;
                int err = pthread_create(&thread, NULL, pindah, (void *)path);
                pthread_join(thread, NULL);
            }fileRekursif(path);
        }

            
    }
    closedir(dir);
}



int main(int argc, char *argv[])
{
	int i=0;
	int err;
	while(i<1) // loop sejumlah thread
	{
		err=pthread_create(&(tid[i]),NULL,&playandcount,NULL); //membuat thread
		if(err!=0) //cek error
		{
			printf("\ncan't create thread : [%s]",strerror(err));
		}
		else
		{
			printf("\ncreate thread success\n\n");
		}
		i++;
	}
	pthread_join(tid[0],NULL);

	char cwd[PATH_MAX];
    //saat ada argument -f
    if (strcmp(argv[1], "-f") == 0)
    {
        pthread_t thread;
        int i;
        //buat thread dan print msg
        for (i = 2; i < argc; i++)
        {
            char printmsg[1000];
            int tersedia = file_c_exists(argv[i]);
            if (tersedia)
            {
                sprintf(printmsg, "File %d : Berhasil Dikategorikan", i - 1);
            }
            else
            {
                sprintf(printmsg, "File %d : Sad, gagal :(", i - 1);
            }
            printf("%s\n", printmsg);
            int err = pthread_create(&thread, NULL, pindah, (void *)argv[i]);
        }
        //join semua thread
        pthread_join(thread, NULL);
    }
    else
    {   //saat ada *
        if (strcmp(argv[1], "*") == 0)
        {
            if (getcwd(cwd, sizeof(cwd)) != NULL)
                //membuka working directory sendiri
                fileRekursif(cwd);
        }
    }
	exit(0);
	return 0;
}


